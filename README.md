# LED Crystal

LED light up crystal based off the Adafruit tutorial [Paper-Craft Crystal Gem Lantern](https://learn.adafruit.com/paper-craft-crystal-gem-lantern). The flame effect code is based off of the Adafruit tutorial [Cosplay Floating LED Fireball](https://learn.adafruit.com/cosplay-fireball-prop-with-motion-sensing). 

The project currently uses a 16 LED Neoring and a  Parallax Propeller chip.
