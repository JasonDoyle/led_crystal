{{
File: crystal_led_ring.spin
Author: Jason Doyle
Year: 2021
}}

CON

  _clkmode = xtal1 + pll16x                                     ' 16x required for WS2812                              
  _xinfreq = 5_000_000                                          ' use 5MHz crystal

  LEDS_PIN = 1                                                  ' LED tx pin

  STRIP_LEN = 16

OBJ

  DEBUG   : "FullDuplexSerial"                              ' Dubugging over serial
  DEMO    : "jm_ws2812_demo"
  fp      : "Float32Full"
  LED     : "jm_ws2812"                                     ' Neopixel Lighting
  RAND    : "RealRandom"


PUB main | pos, ch

  init
  LED.off

  'flame_effect
  
  'repeat 5
  '  DEMO.color_chase(@rainbow, STRIP_LEN, 100)
  
  repeat
    flame_effect


PUB init

  LED.start_b(LEDS_PIN, STRIP_LEN)                          ' start led driver
  DEBUG.Start (31, 30, 0, 115200)
  RAND.start
  FP.start
  'start_tone                                               'Play tone to check for chip reset

PUB start_tone

    ctra[30..26] := %00100
    ctra[5..0] := 27
    frqa := 112_367
 
    dira[27]~~
    waitcnt(clkfreq + cnt)
    frqa := 0
    dira[27]~

PUB error_tone

    ctra[30..26] := %00100
    ctra[5..0] := 27
    frqa := 70786
 
    dira[27]~~
    waitcnt(clkfreq/6+ cnt)
    frqa := 0
    dira[27]~


PUB pause_ms(ms)

  repeat ms
    waitcnt(clkfreq/1000 + cnt)


PUB HSV_to_RGB (h,s,v) : rgb | c, x, m, r1, g1, b1, r, g, b

  '' Converts Hue, Saturation, and Value into a packed rgb long
  '' Hue 0 - 360
  '' Sat 0 - 100
  '' val 0 - 100

  debug.str(String("hsv", 10, 13))
    
  ' Convert h, s, v into floating point numbers
  h := FP.FFloat(h)
  s := FP.FFloat(s)
  v := FP.FFloat(v)

  c := FP.FMul(FP.FDiv(v, 100.0), FP.FDiv(s, 100.0))
  x := FP.FMul(c, FP.FSub(1.0, FP.FAbs(FP.FSub(FP.FMod(FP.FDiv(h, 60.0), 2.0), 1.0))))
  m := FP.FSub(FP.FDiv(v, 100.0), c)
  
  r1 := FP.FFloat(r1)
  b1 := FP.FFloat(b1)
  g1 := FP.FFloat(g1)
  
  if h => 0 AND h < 60.0
    r1 := c
    g1 := x
    b1 := 0
  
  elseif h => 60.0 AND h < 120.0
    r1 := x
    g1 := c
    b1 := 0
    
  elseif h => 120.0 AND h < 180.0
    r1 := 0
    g1 := c
    b1 := x
    
  elseif h => 180.0 AND h < 240.0
    r1 := 0
    g1 := x
    b1 := c
    
  elseif h => 240.0 AND h < 300.0
    r1 := x
    g1 := 0
    b1 := c
    
  elseif h => 300.0 AND h < 360.0
    r1 := c
    g1 := 0
    b1 := x

  r := FP.FMul(FP.FAdd(r1, m), 255.0)
  g := FP.FMul(FP.FAdd(g1, m), 255.0)
  b := FP.FMul(FP.FAdd(b1, m), 255.0)
  
  rgb.byte[2] := FP.FTrunc(r)
  rgb.byte[1] := FP.FTrunc(g)
  rgb.byte[0] := FP.FTrunc(b)


PUB flame_effect | led_num, hue, sat, val, rgb
  '' based on adafruit code https://learn.adafruit.com/cosplay-fireball-prop-with-motion-sensing?view=all
  
  led_num := ||(RAND.random//16)
  hue := ||(RAND.random//10) + 260
  sat := 100
  val := ||(RAND.random//20) + 40
  
  rgb := HSV_to_RGB (hue,sat,val)
  
  
  LED.set(led_num, rgb)
  pause_ms(100)
  
 
  led_num := ||(RAND.random//16)
  hue := ||(RAND.random//10) + 260
  sat := 100
  val := ||(RAND.random//20) + 40
  
  rgb := HSV_to_RGB (hue,sat,val)
  
  
  LED.set(led_num, rgb)
  
  led_num := ||(RAND.random//16)
  LED.set(led_num, 0)
  pause_ms(100)
  
  led_num := ||(RAND.random//16)
  hue := ||(RAND.random//10) + 260
  sat := 100
  val := ||(RAND.random//20) + 40
  
  rgb := HSV_to_RGB (hue,sat,val)
  
  
  LED.set(led_num, rgb)
  
  led_num := ||(RAND.random//16)
  LED.set(led_num, 0)
  pause_ms(100)


{{
Copyright (c) 2021 Jason Doyle

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
}}
