EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1900 1900 1400 1050
U 608598DB
F0 "power" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 1950 3400 1300 950 
U 608604C2
F0 "main" 50
F1 "main.sch" 50
$EndSheet
$EndSCHEMATC
